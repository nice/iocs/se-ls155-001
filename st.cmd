# ---
require lakeshore155
require essioc

epicsEnvSet("IPADDR", "172.30.32.16")
epicsEnvSet("IPPORT", "8888")
epicsEnvSet("LOCATION", "SE: $(IPADDR):$(IPPORT)")
epicsEnvSet("SYS", "SE-SEE")
epicsEnvSet("P", "$(SYS):")
epicsEnvSet("DEV", "SE-LS155-001")
epicsEnvSet("R", "$(DEV):")
epicsEnvSet("PORTNAME", "$(SYS)-$(DEV)")
epicsEnvSet("A", -1)

iocshLoad("$(essioc_DIR)/common_config.iocsh")
iocshLoad("$(lakeshore155_DIR)/lakeshore155.iocsh", "P=$(P), R=$(R), IPADDR=$(IPADDR), IPPORT=$(IPPORT), PORTNAME=$(PORTNAME), A=$(A)")

iocInit()
# ...
